<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/add_student','StudentsController@students')->name('add_student');
Route::get('/view_students','StudentsController@view_students')->name('view_students');

Route::post('/update_student','StudentsController@update')->name('update_student');


Route::get('/delete/{id}',[
'uses'=>'StudentsController@delete',
'as'=>'delete',
]);

Route::get('/edit/{id}',[
    'uses'=>'StudentsController@edit',
    'as'=>'edit',
    ]);

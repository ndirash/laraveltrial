@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Student Information
                <a  class="btn btn-info" href="{{route('view_students')}}">View Students</a>
                
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


 <form role="form" method="POST" action="{{route('update_student')}}">
@include('includes.message')


<input type="hidden" name="_token" value="{{csrf_token()}}">
  <div class="form-group">
    <label for="name">Name:</label>
    <input type="text" name="name" class="form-control" value="{{$students->name}}" id="name">
  </div>
  <div class="form-group">
    <label for="regno">Registration Number:</label>
    <input type="text" name="regno" class="form-control" value="{{$students->regno}}" id="regno">
  </div>
  <div class="form-group">
    <label for="gender">Gender:</label>
   <select id="gender" value="{{$students->gender}}" name="gender">
   <option>--Select--</option>
   <option>Male</option>
   <option>Female</option>
   <option>Others</option>
   </select>
  </div>
  <div class="form-group">
    <label for="phone">Phone Number:</label>
    <input type="text" class="form-control" value="{{$students->phone}}" name="phone" id="phone">
  </div>
  <div class="form-group">
    <label for="age">Age:</label>
    <input type="text" class="form-control" name="age" value="{{$students->age}}" id="age">
  </div>

  <input type="text" name="id" class="form-control" value="{{$students->id}}" hidden id="id">

 
  <button type="submit" name="" class="btn btn-primary">Update</button>
</form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Registered Students
                <a  class="btn btn-info" href="{{route('home')}}">Home(add student)</a>
                </div>
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    @include('includes.message')

                    <div class="table-responsive table-desi">
                        <table class="table table-hover">
                            <thead>
                                <tr>

                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Registration Number</th>
                                    <th>Phone</th>
                                    <th>Gender</th>
                                    <th>Age</th>
                                    <th></th>  
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
@foreach($students as $students)
<tr>
<td>{{$students->id}}</td>
<td>{{$students->name}}</td>
<td>{{$students->regno}}</td>
<td>{{$students->gender}}</td>
<td>{{$students->phone}}</td>
<td>{{$students->age}}</td>
<td></td>
<td> <a class="btn btn-info" href="{{route('edit',['id'=>$students->id])}}">Edit</a></td>
<td> <a class="btn btn-danger" href="{{route('delete',['id'=>$students->id])}}">Delete</a></td>

</tr>
@endforeach

                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
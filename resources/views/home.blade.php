@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Register Students
                <a  class="btn btn-info" href="{{route('view_students')}}">View Students</a>
                
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


 <form role="form" method="POST" action="{{route('add_student')}}">
@include('includes.message')

<input type="hidden" name="_token" value="{{csrf_token()}}">
  <div class="form-group">
    <label for="name">Name:</label>
    <input type="text" name="name" class="form-control" id="name">
  </div>
  <div class="form-group">
    <label for="regno">Registration Number:</label>
    <input type="text" name="regno" class="form-control" id="regno">
  </div>
  <div class="form-group">
    <label for="gender">Gender:</label>
   <select id="gender" name="gender">
   <option>--Select--</option>
   <option>Male</option>
   <option>Female</option>
   <option>Others</option>
   </select>
  </div>
  <div class="form-group">
    <label for="phone">Phone Number:</label>
    <input type="text" class="form-control" name="phone" id="phone">
  </div>
  <div class="form-group">
    <label for="age">Age:</label>
    <input type="text" class="form-control" name="age" id="age">
  </div>
 
  <button type="submit" name="" class="btn btn-primary">Submit</button>
</form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

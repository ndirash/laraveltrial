<?php

namespace App\Http\Controllers;

use App\Students;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    
public function students(Request $request){
    $request->validate([
        'name'=>'required',
        'regno'=>'required',
        'gender'=>'required',
        'phone'=>'required',
        'age'=>'required',

    ]);
    $name=$request->input('name');
    $regno=$request->input('regno');
    $gender=$request->input('gender');
    $phone=$request->input('phone');
    $age=$request->input('age');

    Students::create([
'name'=>$name,
'regno'=>$regno,
'gender'=>$gender,
'phone'=>$phone,
'age'=>$age,

    ]);
return redirect()->back()->with('success','Student registered successfuly');
}
public function view_students(){

    $students=Students::all();
    return view('admin.view_students',compact('students'));
}
public function delete($id){
$student=Students::find($id);
$student->delete();

return redirect()->back()->with('success','Student deleted successfully');
}

public function edit($id){
$student=Students::find($id);
return view('admin.edit')->with('students',$student);

}
public function update(Request $request){

    $request->validate([
        'name'=>'required',
        'regno'=>'required',
        'gender'=>'required',
        'phone'=>'required',
        'age'=>'required',

    ]);
    $id=$request->input('id');
    $name=$request->input('name');
    $regno=$request->input('regno');
    $gender=$request->input('gender');
    $phone=$request->input('phone');
    $age=$request->input('age');

    $student=Students::find($id);
    if(!$student){

        return redirect()->back()->with('error','student does not exist');

    }
    
    $student->name=$name;
    $student->regno=$regno;
    $student->gender=$gender;
    $student->phone=$phone;
    $student->age=$age;
    $student->update();

    return redirect()->back()->with('success','student details updated successfully');
}

}
